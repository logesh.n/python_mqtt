import paho.mqtt.client as mqtt
import time


# callbacks
def on_connect(client, userdata, flags, rc):
    print("connected with the code: " + str(rc))

    # subscribe topic
    client.subscribe("Test/#")


def on_message(client, userdata, msg):
    print(str(msg.payload))


client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect('192.168.0.220', 1883, 60)


# client.loop_forever()
client.loop_start()
time.sleep(5)
while True:
    client.publish("Hello/", "Hello world")
    time.sleep(30)

client.loop_stop()
client.disconnect()
